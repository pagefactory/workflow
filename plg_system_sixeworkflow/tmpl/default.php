<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;
?>
<div class="user-center" id="workflow-user-center">

	<div class="user-avatar"><img class="avatar" src="<?php echo JUri::root(true); ?>/plugins/system/sixeworkflow/assets/images/joomla.jpg"/><span class="total" style="display:none;"></span></div>
	<div class="user-center-pane">

		<div class="user-center-main">
			<div class="user-center-top">
				<h5><?php echo JText::_('PLG_SYSTEM_SIXEWORKFLOW_MENU_USERNAME'); ?><?php echo $user->username; ?></h5>
				<div class="add-button">
					<span><?php echo JText::_('PLG_SYSTEM_SIXEWORKFLOW_MENU_USERCENTER'); ?></span>
					<a href="<?php echo JRoute::_('index.php?option=com_sixeworkflow&view=articleform&a_id=0&layout=edit'); ?>"><?php echo JText::_('PLG_SYSTEM_SIXEWORKFLOW_MENU_NEW_ARTICLE'); ?></a>
				</div>
			</div>
			<div class="user-center-menus">
				<?php echo JLayoutHelper::render('menu', $menus, $this->formpath . '/layouts'); ?>
			</div>
			<div class="user-center-bottom">
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=login&layout=logout&task=user.menulogout'); ?>"><?php echo JText::_('PLG_SYSTEM_SIXEWORKFLOW_MENU_LOGOUT'); ?></a>
			</div>
		</div>
	</div>
</div>