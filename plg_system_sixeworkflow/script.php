<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die();

class plgSystemSixeWorkFlowInstallerScript
{
	/**
	 * Called after any type of action
	 *
	 * @param     string $route Which action is happening (install|uninstall|discover_install)
	 * @param     jadapterinstance $adapter The object responsible for running this script
	 *
	 * @return    boolean                         True on success
	 */
	public function postflight($route, JAdapterInstance $adapter)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query
			->update('#__extensions')
			->set("enabled='1'")
			->set("ordering='10'")
			->where("type='plugin'")
			->where("folder='system'")
			->where("element='sixeworkflow'");
		$db->setQuery($query);
		$db->execute();

		return true;
	}
}
