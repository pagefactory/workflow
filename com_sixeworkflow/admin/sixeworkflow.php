<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');

if (!JFactory::getUser()->authorise('core.manage', 'com_sixeworkflow'))
{
	throw new JAccessExceptionNotallowed(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}
jimport('joomla.application.component.controller');

JLoader::registerPrefix('SixeWorkFlow', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('SixeWorkFlowHelper', __DIR__ . '/helpers/workflow.php');
$controller = JControllerLegacy::getInstance('SixeWorkFlow');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
