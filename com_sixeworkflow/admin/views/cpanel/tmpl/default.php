<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;
use Joomla\CMS\Uri\Uri;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('formbehavior.chosen', '.multipleTags', null, array('placeholder_text_multiple' => JText::_('JOPTION_SELECT_TAG')));

JHtml::_('formbehavior.chosen', 'select');

$app       = JFactory::getApplication();
$user      = JFactory::getUser();
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_sixeworkflow/assets/css/workflow.css');
$uri = (string) Uri::getInstance();
		$return = urlencode(base64_encode($uri));
$assoc = JLanguageAssociations::isEnabled();
?>


<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10" >
<?php else : ?>
	<div id="j-main-container ">
<?php endif; ?>
	<div class="workflow-cpanel" >
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=workflows" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/workflow.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_WORKFLOWS_TITLE');?></a>
		</div>
		<div class="well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=articles&filter_published=1" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/complete.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_STATE_1_TITLE');?></a>
		</div>
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=articles&filter_published=4" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/submit.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_STATE_4_TITLE');?></a>
		</div>
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=articles&filter_published=5" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/reject.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_STATE_5_TITLE');?></a>
		</div>
		<div style="clear:both" ></div>
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=messages" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/message.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_TITLE');?></a>
		</div>
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_config&view=component&component=com_sixeworkflow&path=&return=<?php echo $return;?>" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/config.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_CONFIG_TITLE');?></a>
		</div>
		<div class=" well-small  cpanel-item">
			<a href="index.php?option=com_sixeworkflow&view=help" ><img src="<?php echo JUri::root(true);?>/administrator/components/com_sixeworkflow/assets/image/help.svg" /><?php echo JText::_('COM_SIXEWORKFLOW_HELP_TITLE');?></a>
		</div>
	</div>
	</div>

