<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('formbehavior.chosen', '.multipleTags', null, array('placeholder_text_multiple' => JText::_('JOPTION_SELECT_TAG')));

JHtml::_('formbehavior.chosen', 'select');

$app       = JFactory::getApplication();
$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';
$columns   = 10;


	$orderingColumn = 'created';


if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_sixeworkflow&task=messages.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'MessageList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$assoc = JLanguageAssociations::isEnabled();
?>

<form action="<?php echo JRoute::_('index.php?option=com_sixeworkflow&view=messages'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="WorkflowList">
				<thead>
					<tr>

						<th width="1%" class="center">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th style="min-width:100px" class="nowrap">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIXEWORKFLOW_MESSAGES_HEADING_TITLE', 'a.title', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap hidden-phone">
							<?php echo JHtml::_('searchtools.sort',  'COM_SIXEWORKFLOW_MESSAGES_HEADING_TYPE', 'a.type', $listDirn, $listOrder); ?>
						</th>
						<th  class="nowrap ">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIXEWORKFLOW_MESSAGES_HEADING_FROM_USER', 'a.from_name', $listDirn, $listOrder); ?>
						</th>
						<th  class="nowrap ">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIXEWORKFLOW_MESSAGES_HEADING_CREATED', 'a.created', $listDirn, $listOrder); ?>
						</th>
						<th  class="nowrap ">
							<?php echo JHtml::_('searchtools.sort', 'COM_SIXEWORKFLOW_MESSAGES_HEADING_IS_READ', 'a.is_read', $listDirn, $listOrder); ?>
						</th>
						<th  class="nowrap ">
							<?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_HEADING_ACTION'); ?>
						</th>

					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="<?php echo $columns; ?>">
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$item->max_ordering = 0;
					$ordering   = ($listOrder == 'a.ordering');

					?>
					<tr class="row<?php echo $i % 2; ?>" >
						
						<td class="center">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>

						<td class="has-context">
							<div class="pull-left break-word">
								
									<a class="hasTooltip" <?php echo $item->is_read ?'' :'style="font-weight: bold;"';?> href="<?php echo JRoute::_('index.php?option=com_sixeworkflow&view=message&id=' . $item->id); ?>" >
									<?php echo $this->escape($item->title); ?></a>


							</div>
						</td>

						<td class="small">
							<?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_TYPE_'.$item->type); ?>
								
						</td>
						<td class="small">
							<?php echo $item->from_name; ?>
								
						</td>
						<td class="nowrap small hidden-phone">
							<?php
							$date = $item->created;
							echo $date > 0 ? JHtml::_('date', $date, JText::_('DATE_FORMAT_LC4')) : '-';
							?>
						</td>
						<td class="small">
							<?php echo $item->is_read ? JText::_('JYES') : JText::_('JNO'); ?>
								
						</td>

						
						<td class="hidden-phone">
							<a onclick="return confirm('<?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_DELECT_CONFIRM');?>');" href="<?php echo JRoute::_('index.php?option=com_sixeworkflow&task=message.delete&id='.$item->id);?>"><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_ACTION_DELECT');?></a>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php // Load the batch processing form. ?>
			<?php if ($user->authorise('core.create', 'com_content')
				&& $user->authorise('core.edit', 'com_content')
				&& $user->authorise('core.edit.state', 'com_content')) : ?>
				<?php echo JHtml::_(
					'bootstrap.renderModal',
					'collapseModal',
					array(
						'title'  => JText::_('COM_CONTENT_BATCH_OPTIONS'),
						'footer' => $this->loadTemplate('batch_footer'),
					),
					$this->loadTemplate('batch_body')
				); ?>
			<?php endif; ?>
		<?php endif; ?>

		<?php echo $this->pagination->getListFooter(); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
