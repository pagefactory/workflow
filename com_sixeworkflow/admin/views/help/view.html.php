<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
defined('_JEXEC') or die;

/**
 * View class for a list of articles.
 *
 * @since  1.6
 */
class SixeWorkFlowViewHelp extends JViewLegacy
{
	
	protected $sidebar;
	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise an Error object.
	 */
	public function display($tpl = null)
	{
		SixeWorkflowHelper::addSubmenu('help');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		$lang=JFactory::getLanguage();
		$this->setLayout($lang->getTag());
		
		return parent::display($tpl);
	}

	protected function addToolbar()
	{
		$canDo = SixeWorkFlowHelper::getActions();
		$user  = JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolbar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_SIXEWORKFLOW_HELP_TITLE'), 'cog');

		


	}


}
