<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root() . 'administrator/components/com_sixeworkflow/assets/css/workflow.css');

$lang = JFactory::getLanguage()->getTag();
$noticeApi = 'https://www.eee-eee.com/index.php?option=com_sixedev&view=ajax&format=json&language=' . $lang;
$ver = SixeWorkFlowHelper::getVersion();

// Add the script to the document head.
$doc->addScriptDeclaration("
	jQuery(function ($) { 
		if ($('#sixeworkflow-help').length > 0) {
			$.ajax({
				type: 'POST', 
				url: '" . $noticeApi . "', 
				data: {
					'id': 13,
					'action': 'help', 
					'api_host': '" . urlencode($_SERVER['SERVER_NAME']) . "',
					'ver': '" . $ver . "'
				},
				dataType: 'jsonp',
				success: function(res){
					$('#sixeworkflow-help').html(res.status ? res.data : '');
				}
			});
		}
	});
");
?>

<?php if (!empty($this->sidebar)) : ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
	<?php else : ?>
	<div id="j-main-container ">
		<?php endif; ?>
		<div class="sixeworkflow-help" id="sixeworkflow-help">
			<p>There is no help message.</p>
		</div>
	</div>

