<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.formvalidation');

JHtml::_('formbehavior.chosen', '.group-list', null, array('placeholder_text_multiple' => JText::_('FLOWS_SELECT_GRUOP')));
JHtml::_('formbehavior.chosen', '.user-list', null, array('placeholder_text_multiple' => JText::_('FLOWS_SELECT_USER')));
JHtml::_('formbehavior.chosen', '.multipleCategories', null, array('placeholder_text_multiple' => JText::_('COM_SIXEWORKFLOW_FORM_WORKFLOW_CATEGORY_SELECT_CATEGORY')));
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();


?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task,type) {
		if (task == 'workflow.cancel') {
			Joomla.submitform(task, document.getElementById('workflow-form'));
		}
		else {
			
			if (task != 'workflow.cancel' && document.formvalidator.isValid(document.id('workflow-form'))) {
				
				Joomla.submitform(task, document.getElementById('workflow-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_sixeworkflow&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="workflow-form" class="form-validate workflow-form">
			<?php echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
	<div class="form-horizontal">
		<div class="span9">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<?php echo JHtml::_('bootstrap.addTab', 'myTab', $fieldset->name, JText::_($fieldset->label, true)); ?>
			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid form-horizontal-desktop">
						<?php echo  $this->form->renderFieldset($fieldset->name);?>
					</div>
				</div>
			</div>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endforeach; ?>

		<?php if (JFactory::getUser()->authorise('core.admin','rms')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		</div>
		<div class="span3">
				<?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
		</div>
		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
