<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
// No direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');



if($this->item->message=='')
{
    $this->item->message=JText::_('COM_SIXEWORKFLOW_FORM_NOMESSAGE');
}

?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task,type) {
		
		Joomla.submitform(task, document.getElementById('message-form'));
		

	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_sixeworkflow&view=message&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="message-form" class="form-validate message-form">
			

<input type="hidden" name="task" value=""/>
<?php echo JHtml::_('form.token'); ?>
</form>


<div class="workflow-content">


<div class="message workflow-main">
<table class="table table-striped table-bordered table-hover ">
<thead><tr><th><?php echo $this->item->title;?></th></tr></thead>
<tbody>
	<tr>
	 <td>发送人</td>
	 <td><?php echo $this->item->from_name;?></td>
	</tr>
	<tr>
	 <td>收件人</td>
	 <td><?php echo $this->item->to_name;?></td>
	</tr>
	<tr>
	 <td>状态</td>
	 <td><?php echo $this->item->is_read ? JText::_('COM_SIXEWORKFLOW_FILTER_STATE_READ') : JText::_('COM_SIXEWORKFLOW_FILTER_STATE_NO_READ') ;?></td>
	</tr>
	<tr>
	 <td>日期</td>
	 <td><?php echo JHtml::_('date',$this->item->created,JText::_('Y-m-d H:i:s')) ;?></td>
	</tr>

	<tr>
	 <td>消息类型</td>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_FILTER_TYPE_OPTION_'.$this->item->type);?></td>
	</tr>

	<tr>
	 <td>消息内容</td>
	 <td><?php echo $this->item->message;?></td>
	</tr>
</tbody>
</table>
</div>

</div>



