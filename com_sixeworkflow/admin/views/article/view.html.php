<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;
JLoader::register('SixeWorkFlowHelperRoute', JPATH_SITE . '/components/com_sixeworkflow/helpers/route.php');
JLoader::register('ContentViewArticle', JPATH_ADMINISTRATOR . '/components/com_content/views/article/view.html.php');
/**
 * View to edit an article.
 *
 * @since  1.6
 */
class SixeWorkFlowViewArticle extends ContentViewArticle
{
	
public function __construct($config = array())
	{
		parent::__construct($config);
		$lang = JFactory::getLanguage();
		$lang->load('com_content', JPATH_ADMINISTRATOR, null, false, true);

	}



	/**
	 * The JForm object
	 *
	 * @var  JForm
	 */
	protected $form;

	/**
	 * The active item
	 *
	 * @var  object
	 */
	protected $item;

	/**
	 * The model state
	 *
	 * @var  object
	 */
	protected $state;

	/**
	 * The actions the user is authorised to perform
	 *
	 * @var  JObject
	 */
	protected $canDo;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
		$this->userflow=$this->get('UserFlow');

		

		return parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user       = JFactory::getUser();
		$userId     = $user->id;
		$isNew      = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);

		// Built the actions for new and existing records.
		$canDo = $this->canDo;
		$bar = JToolbar::getInstance('toolbar');
		JToolbarHelper::title(
			JText::_('COM_CONTENT_PAGE_' . ($checkedOut ? 'VIEW_ARTICLE' : ($isNew ? 'ADD_ARTICLE' : 'EDIT_ARTICLE'))),
			'pencil-2 article-add'
		);

		// For new records, check the create permission.
		if ($isNew && (count($user->getAuthorisedCategories('com_content', 'core.create')) > 0)  )
		{
			//JToolbarHelper::apply('article.apply');
			//JToolbarHelper::save('article.save');
			//JToolbarHelper::save2new('content.save2new');
			JToolbarHelper::cancel('article.cancel');
		}
		else
		{
			// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
			$itemEditable = $canDo->get('core.edit') || ($canDo->get('core.edit.own'));

			// Can't save the record if it's checked out and editable
			if (!$checkedOut && $itemEditable && in_array($this->item->flow,$this->userflow))
			{
				//JToolbarHelper::apply('article.apply');
				//JToolbarHelper::save('article.save');

				// We can save this record, but check the create permission to see if we can return to make a new one.
				if ($canDo->get('core.create'))
				{
					//JToolbarHelper::save2new('content.save2new');
				}
			}
			// If checked out, we can still save
			$pview_link=JRoute::_(JUri::root().SixeWorkFlowHelperRoute::getArticleRoute($this->item->id, $this->item->catid, $this->item->language));
			$pview = '<a class="btn btn-small" href="'.$pview_link.'"  target="_blank">'.JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_PREVIEW').'</a>';
			$bar->appendButton('Custom', $pview, 'pview');

			JToolbarHelper::custom('article.complete','publish.png','publish.png', 'COM_SIXEWORKFLOW_FORM_TOOLBAR_COMPLETE', false);
			JToolbarHelper::custom('article.reset','unpublish.png','unpublish.png', 'COM_SIXEWORKFLOW_FORM_TOOLBAR_RESET', false);

			if (JComponentHelper::isEnabled('com_contenthistory') && $this->state->params->get('save_history', 0) && $itemEditable)
			{
				JToolbarHelper::versions('com_content.article', $this->item->id);
			}


			JToolbarHelper::cancel('article.cancel', 'JTOOLBAR_CLOSE');
		}
		JToolbarHelper::divider();
		
		if(in_array($this->item->flow,$this->userflow) && !$isNew)
		{
			if($this->item->flow < $this->item->max_flow)
			{
				//JToolbarHelper::custom('article.submit', 'share', 'share', 'COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_SUBMIT', false, false);
				
			}

			if($this->item->flow > $this->item->start_flow)
			{
				//JToolbarHelper::custom('content.reject', 'reply', 'reply', 'COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_REJECT', false, false);

			$dhtml = '<button type="button" data-toggle="modal" onclick="jQuery( \'#collapseModal\' ).modal(\'show\'); return true;" class="btn btn-small">
	<span class="icon-reply" aria-hidden="true"></span>'.JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_REJECT').'
	
	</button>';
			//$bar->appendButton('Custom', $dhtml, 'reply');
			}
		

			if($this->item->flow==$this->item->max_flow)
			{
			//	JToolbarHelper::custom('article.complete', 'ok', 'ok', 'COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_COMPLETE', false, false);
			}
		}
		
		

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_CONTENT_ARTICLE_MANAGER_EDIT');
	}
}
