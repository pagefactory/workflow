<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
defined('_JEXEC') or die;

?>

<div class="container-fluid">
	<div class="row-fluid">
		<textarea id="jform_message" name="jform[message]" rows="5" cols="20" class="span8"><?php echo $this->item->message;?></textarea>
	</div>

</div>
