<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
defined('_JEXEC') or die;

?>
<button type="button" class="btn" data-dismiss="modal">
	<?php echo JText::_('JCANCEL'); ?>
</button>
<button type="button" class="btn btn-success" onclick="Joomla.submitbutton('article.reject');">
	<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
</button>
