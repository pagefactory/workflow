<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */



defined('JPATH_PLATFORM') or die;
use Joomla\Utilities\ArrayHelper;


//JFormHelper::loadFieldClass('list');

/**
 * Field to load a dropdown list of available user groups
 *
 * @since  3.2
 */
class JFormFieldMessages extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   3.2
	 */
	protected $type = 'Messages';

	

	/**
	 * Method to attach a JForm object to the field.
	 *
	 * @param   \SimpleXMLElement  $element  The SimpleXMLElement object representing the `<field>` tag for the form field object.
	 * @param   mixed              $value    The form field value to validate.
	 * @param   string             $group    The field name group control value. This acts as an array container for the field.
	 *                                       For example if the field has name="foo" and the group value is set to "bar" then the
	 *                                       full field name would end up being "bar[foo]".
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7.0
	 */
	public function setup(SimpleXMLElement $element, $value, $group = null)
	{
	
		return parent::setup($element, $value, $group);
	}

	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	protected function getOptions()
	{
		$options = array();
		$app=JFactory::getApplication();
		$user=JFactory::getUser();
		$id=$app->input->get('id',0);
		if($app->input->get('option','') =='com_sixeworkflow' && $app->input->get('view','') == 'article' && $id > 0 )
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('DISTINCT a.*,u.name,wu.title AS flow_title,c.start_flow')
				  ->from('#__workflow_messages AS a')
				  ->join('LEFT','#__users AS u ON u.id=a.from_user')
				  ->join('LEFT','#__workflow_contents AS c ON c.content_id=a.content_id')
				  ->join('LEFT','#__workflow_categories AS wc ON wc.category_id=c.catid')
				  ->join('LEFT','#__workflow_users AS wu ON (wu.workflow_id=wc.workflow_id) AND (wu.flow_id=a.flow)')

				  ->where('a.content_id='.(int)$id)
				  //->where('a.to_user='.(int)$user->id)
				  ->where('a.type=2');
				  $query->order('a.id DESC');
				  $db->setQuery($query);

				try
				{
					$options = $db->loadObjectList();
				}
				catch (RuntimeException $e)
				{
					JError::raiseWarning(500, $e->getMessage());
				}

		}
		
		return $options;
	}

	protected function getInput()
	{
		$options = (array) $this->getOptions();
		
		
		$html=array();
		$html[]='<table class="table table-striped message-list">';
		$html[]='<thead>';
		$html[]='<tr>';
		$html[]='<th width="15%" class="nowrap">'.JText::_('COM_SIXEWORKFLOW_FORM_MESSAGE_LIST_FROM').'</th>';
		$html[]='<th width="15%" class="nowrap">'.JText::_('COM_SIXEWORKFLOW_FORM_MESSAGE_LIST_FLOW').'</th>';
		$html[]='<th width="55%" class="nowrap">'.JText::_('COM_SIXEWORKFLOW_FORM_MESSAGE_LIST_TEXT').'</th>';
		$html[]='<th width="15%" class="nowrap">'.JText::_('COM_SIXEWORKFLOW_FORM_MESSAGE_LIST_DATE').'</th>';
		$html[]='</tr></thead>';
		$html[]='<tbody>';
		foreach($options as $key=>$option)
		{
	
			if($option->message=='')
			{
				$option->message=JText::_('COM_SIXEWORKFLOW_FORM_NOMESSAGE');
			}
			$html[]='<tr>';
			$html[]='<td>'.$option->name.'</td>';
			$html[]='<td>'.$option->flow_title.'</td>';
			$html[]='<td>'.$option->message .'</td>';
			$html[]='<td>'.JHtml::_('date',$option->created,JText::_('Y-m-d H:i:s')) .'</td>';
			$html[]='</tr>';
		}
		$html[]='</tbody>';
		$html[]='</table>';
		return implode($html);
	}



}
