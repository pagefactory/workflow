DROP TABLE IF EXISTS `#__workflows`;
DROP TABLE IF EXISTS `#__workflow_categories`;
DROP TABLE IF EXISTS `#__workflow_contents`;
DROP TABLE IF EXISTS `#__workflow_flows`;
DROP TABLE IF EXISTS `#__workflow_messages`;
DROP TABLE IF EXISTS `#__workflow_users`;
