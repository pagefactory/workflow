

CREATE TABLE IF NOT EXISTS `#__workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `introtext` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL,
  `state` tinyint(3)  NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `#__workflow_categories` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`) USING BTREE,
  KEY `workflow_id` (`workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `#__workflow_contents` (
  `content_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `flow` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `start_flow` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rejected_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `rejected` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `catid` (`catid`),
  KEY `state` (`state`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------



CREATE TABLE IF NOT EXISTS `#__workflow_flows` (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `flow_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`content_id`,`flow_id`,`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------



CREATE TABLE IF NOT EXISTS `#__workflow_messages` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(3) UNSIGNED NOT NULL DEFAULT '0',
  `from_user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `to_user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `flow` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_read` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_send` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`to_user`,`is_read`,`is_send`),
  KEY `user_content` (`content_id`,`to_user`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------



CREATE TABLE IF NOT EXISTS `#__workflow_users` (
  `workflow_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `flow_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  UNIQUE KEY `workflow` (`workflow_id`,`type`,`user_id`) USING BTREE,
  KEY `workflow_id` (`workflow_id`,`flow_id`),
  KEY `type` (`type`,`user_id`,`flow_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

