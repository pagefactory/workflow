<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\CMS\Uri\Uri;

/**
 * Content component helper.
 *
 * @since  1.6
 */
class SixeWorkFlowHelper
{
	public static $extension = 'com_sixeworkflow';

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string $vName The name of the active view.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public static function addSubmenu($vName)
	{

		$uri = (string)Uri::getInstance();
		$return = urlencode(base64_encode($uri));

		JHtmlSidebar::addEntry('<span class="icon-cog"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_CPANEL_TITLE'),
			'index.php?option=com_sixeworkflow&view=cpanel',
			$vName == 'cpanel'
		);

		JHtmlSidebar::addEntry('<span class="icon-shuffle"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_WORKFLOWS_TITLE'),
			'index.php?option=com_sixeworkflow&view=workflows',
			$vName == 'workflows'
		);


		JHtmlSidebar::addEntry('<span class="icon-publish"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_STATE_1_TITLE'),
			'index.php?option=com_sixeworkflow&view=articles&filter_published=1',
			$vName == 'articles_state1'
		);
		/*
		JHtmlSidebar::addEntry('<span class="icon-edit"></span>  '.
			JText::_('COM_SIXEWORKFLOW_STATE_3_TITLE'),
			'index.php?option=com_sixeworkflow&view=articles&filter_published=3',
			$vName == 'articles_state3'
		);
		*/
		JHtmlSidebar::addEntry('<span class="icon-notification-circle"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_STATE_4_TITLE'),
			'index.php?option=com_sixeworkflow&view=articles&filter_published=4',
			$vName == 'articles_state4'
		);
		JHtmlSidebar::addEntry('<span class="icon-reply"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_STATE_5_TITLE'),
			'index.php?option=com_sixeworkflow&view=articles&filter_published=5',
			$vName == 'articles_state5'
		);

		JHtmlSidebar::addEntry('<span class="icon-comments-2"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_MESSAGES_TITLE'),
			'index.php?option=com_sixeworkflow&view=messages',
			$vName == 'messages'
		);
		JHtmlSidebar::addEntry('<span class="icon-cog"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_CONFIG_TITLE'),
			'index.php?option=com_config&view=component&component=com_sixeworkflow&path=&return=' . $return,
			$vName == 'config'
		);
		JHtmlSidebar::addEntry('<span class="icon-question-sign"></span>  ' .
			JText::_('COM_SIXEWORKFLOW_HELP_TITLE'),
			'index.php?option=com_sixeworkflow&view=help',
			$vName == 'help'
		);


	}


	public static function getActions()
	{
		$user = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_sixeworkflow';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}

	/**
	 * Applies the content tag filters to arbitrary text as per settings for current user group
	 *
	 * @param   text $text The string to filter
	 *
	 * @return  string  The filtered string
	 *
	 * @deprecated  4.0  Use JComponentHelper::filterText() instead.
	 */
	public static function filterText($text)
	{
		try {
			JLog::add(
				sprintf('%s() is deprecated. Use JComponentHelper::filterText() instead', __METHOD__),
				JLog::WARNING,
				'deprecated'
			);
		} catch (RuntimeException $exception) {
			// Informational log only
		}

		return JComponentHelper::filterText($text);
	}

	/**
	 * Adds Count Items for Category Manager.
	 *
	 * @param   stdClass[]  &$items The category objects
	 *
	 * @return  stdClass[]
	 *
	 * @since   3.5
	 */
	public static function countItems(&$items)
	{
		$config = (object)array(
			'related_tbl' => 'content',
			'state_col' => 'state',
			'group_col' => 'catid',
			'relation_type' => 'category_or_group',
		);

		return parent::countRelations($items, $config);
	}

	/**
	 * Adds Count Items for Tag Manager.
	 *
	 * @param   stdClass[]  &$items The tag objects
	 * @param   string $extension The name of the active view.
	 *
	 * @return  stdClass[]
	 *
	 * @since   3.6
	 */
	public static function countTagItems(&$items, $extension)
	{
		$parts = explode('.', $extension);
		$section = count($parts) > 1 ? $parts[1] : null;

		$config = (object)array(
			'related_tbl' => ($section === 'category' ? 'categories' : 'content'),
			'state_col' => ($section === 'category' ? 'published' : 'state'),
			'group_col' => 'tag_id',
			'extension' => $extension,
			'relation_type' => 'tag_assigments',
		);

		return parent::countRelations($items, $config);
	}

	/**
	 * Returns a valid section for articles. If it is not valid then null
	 * is returned.
	 *
	 * @param   string $section The section to get the mapping for
	 *
	 * @return  string|null  The new section
	 *
	 * @since   3.7.0
	 */
	public static function validateSection($section)
	{
		if (JFactory::getApplication()->isClient('site')) {
			// On the front end we need to map some sections
			switch ($section) {
				// Editing an article
				case 'form':

					// Category list view
				case 'featured':
				case 'category':
					$section = 'article';
			}
		}

		if ($section != 'article') {
			// We don't know other sections
			return null;
		}

		return $section;
	}

	/**
	 * Returns valid contexts
	 *
	 * @return  array
	 *
	 * @since   3.7.0
	 */
	public static function getContexts()
	{
		JFactory::getLanguage()->load('com_content', JPATH_ADMINISTRATOR);

		$contexts = array(
			'com_content.article' => JText::_('COM_CONTENT'),
			'com_content.categories' => JText::_('JCATEGORY')
		);

		return $contexts;
	}

	/**
	 * Get Component Version
	 */
	public static function getVersion()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select('manifest_cache')
			->from($db->qn('#__extensions'))
			->where($db->qn('element') . ' = ' . $db->q('com_sixeworkflow'));
		$db->setQuery($query);
		$manifest_cache = json_decode($db->loadResult());
		if (isset($manifest_cache->version) && $manifest_cache->version) {
			return $manifest_cache->version;
		}
		return '0.0.0';
	}
}
