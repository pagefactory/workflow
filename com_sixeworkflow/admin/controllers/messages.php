<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Articles list controller class.
 *
 * @since  1.6
 */
class SixeWorkFlowControllerMessages extends JControllerAdmin
{
	
	
	var $text_prefix="COM_SIXEWORKFLOW_MESSAGES";
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  The array of possible config values. Optional.
	 *
	 * @return  JModelLegacy
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Message', $prefix = 'SixeWorkFlowModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}
	public function delete()
	{
		// Check for request forgeries
		$this->checkToken();

		// Get items to remove from the request.
		$cid = $this->input->get('cid', array(), 'array');

		if (!is_array($cid) || count($cid) < 1)
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{

			$cid = ArrayHelper::toInteger($cid);
			$user = JFactory::getUser();
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query = $db->getQuery(true);
			$query->delete($query->qn('#__workflow_messages'))
				->where('id IN('.implode(',',$cid).')');
			$db->setQuery($query);
			$return=(boolean)$db->execute();

			// Remove the items.
			if ($return)
			{
				$this->setMessage(\JText::plural('COM_SIXEWORKFLOW_MESSAGES_N_ITEMS_DELETED', count($cid)));
			}
			else
			{
				$this->setMessage(JTEXT::_('COM_SIXEWORKFLOW_MESSAGES_DELETE_ERROR'), 'error');
			}
		}

		$this->setRedirect(JRoute::_('index.php?option=com_sixeworkflow&view=messages', false));
	}

	public function read()
	{
		// Check for request forgeries
		$this->checkToken();

		// Get items to remove from the request.
		$cid = $this->input->get('cid', array(), 'array');

		if (!is_array($cid) || count($cid) < 1)
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{

			$cid = ArrayHelper::toInteger($cid);
			$user = JFactory::getUser();
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query = $db->getQuery(true);
			$query->update($query->qn('#__workflow_messages'))
			      ->set('is_read=1')
				->where('id IN('.implode(',',$cid).')');
			$db->setQuery($query);
			$return=(boolean)$db->execute();

			// Remove the items.
			if ($return)
			{
				$this->setMessage(\JText::plural('COM_SIXEWORKFLOW_MESSAGES_N_ITEMS_READED', count($cid)));
			}
			else
			{
				$this->setMessage(JTEXT::_('COM_SIXEWORKFLOW_MESSAGES_READED_ERROR'), 'error');
			}
		}

		$this->setRedirect(JRoute::_('index.php?option=com_sixeworkflow&view=messages', false));
	}

	public function noread()
	{
		// Check for request forgeries
		$this->checkToken();

		// Get items to remove from the request.
		$cid = $this->input->get('cid', array(), 'array');

		if (!is_array($cid) || count($cid) < 1)
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{

			$cid = ArrayHelper::toInteger($cid);
			$user = JFactory::getUser();
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query = $db->getQuery(true);
			$query->update($query->qn('#__workflow_messages'))
			      ->set('is_read=0')
				->where('id IN('.implode(',',$cid).')');
			$db->setQuery($query);
			$return=(boolean)$db->execute();

			// Remove the items.
			if ($return)
			{
				$this->setMessage(\JText::plural('COM_SIXEWORKFLOW_MESSAGES_N_ITEMS_NOREADED', count($cid)));
			}
			else
			{
				$this->setMessage(JTEXT::_('COM_SIXEWORKFLOW_MESSAGES_NOREADED_ERROR'), 'error');
			}
		}

		$this->setRedirect(JRoute::_('index.php?option=com_sixeworkflow&view=messages', false));
	}
}
