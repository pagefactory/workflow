<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Articles list controller class.
 *
 * @since  1.6
 */
class SixeWorkFlowControllerArticles extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array $config An optional associative array of configuration settings.
	 *
	 * @see     JControllerLegacy
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		// Articles default form can come from the articles or featured view.
		$this->registerTask('complete', 'publish');

		// Value = -2
		$this->registerTask('reset', 'publish');

		$this->registerTask('unfeatured', 'featured');
	}

	/**
	 * Method to toggle the featured setting of a list of articles.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function featured()
	{
		// Check for request forgeries
		$this->checkToken();

		$user = JFactory::getUser();
		$ids = $this->input->get('cid', array(), 'array');
		$values = array('featured' => 1, 'unfeatured' => 0);
		$task = $this->getTask();
		$value = ArrayHelper::getValue($values, $task, 0, 'int');

		// Access checks.
		foreach ($ids as $i => $id) {
			if (!$user->authorise('core.edit.state', 'com_content.article.' . (int)$id)) {
				// Prune items that you can't change.
				unset($ids[$i]);
				JError::raiseNotice(403, JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'));
			}
		}

		if (empty($ids)) {
			JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
		} else {
			// Get the model.
			/** @var ContentModelArticle $model */
			$model = $this->getModel();

			// Publish the items.
			if (!$model->featured($ids, $value)) {
				JError::raiseWarning(500, $model->getError());
			}

			if ($value == 1) {
				$message = JText::plural('COM_CONTENT_N_ITEMS_FEATURED', count($ids));
			} else {
				$message = JText::plural('COM_CONTENT_N_ITEMS_UNFEATURED', count($ids));
			}
		}

		$view = $this->input->get('view', '');

		$this->setRedirect(JRoute::_('index.php?option=com_sixeworkflow&view=articles', false), $message);

	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string $name The model name. Optional.
	 * @param   string $prefix The class prefix. Optional.
	 * @param   array $config The array of possible config values. Optional.
	 *
	 * @return  JModelLegacy
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Article', $prefix = 'SixeWorkFlowModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}


	public function publish()
	{
		// Check for request forgeries
		$this->checkToken();
		// Get items to publish from the request.
		$cid = $this->input->get('cid', array(), 'array');

		$data = array('complete' => 1, 'reset' => 0);
		$task = $this->getTask();
		$value = ArrayHelper::getValue($data, $task, 0, 'int');
		if (empty($cid)) {
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		} else {
			// Get the model.
			$model = $this->getModel();

			// Make sure the item ids are integers
			$cid = ArrayHelper::toInteger($cid);

			// Publish the items.
			try {
				$model->publish($cid, $value);
				$errors = $model->getErrors();
				$ntext = null;

				if ($errors) {
					\JFactory::getApplication()->enqueueMessage(\JText::plural($this->text_prefix . '_N_ITEMS_FAILED_RESET', count($cid)), 'error');
				} else {
					$ntext = $this->text_prefix . '_N_ITEMS_' . $task;
				}

				if ($ntext !== null) {
					$this->setMessage(\JText::plural($ntext, count($cid)));
				}
			} catch (\Exception $e) {
				$this->setMessage($e->getMessage(), 'error');
			}
		}

		$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list, false));
	}

}
