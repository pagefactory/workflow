<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * The article controller
 *
 * @since  1.6
 */
class SixeWorkFlowControllerArticle extends JControllerForm
{
	/**
	 * Class constructor.
	 *
	 * @param   array  $config  A named array of configuration variables.
	 *
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask('complete', 'publish');

		
		$this->registerTask('reset', 'publish');

	}




public function publish()
	{
		// Check for request forgeries
		$this->checkToken();
		$option=$this->input->getCmd('option','');
		$view=$this->input->getCmd('view','');
		// Get items to publish from the request.
		$cid = $this->input->get('cid', array(), 'array');
		if($this->option==$option && $this->view==$view)
		{
			$id=$this->input->getInt('id',0);
			if($id > 0 )
			{
				$cid[]=$id;
			}
		}
		$data = array('complete' => 1, 'reset' => 0);
		$task = $this->getTask();
		$value = ArrayHelper::getValue($data, $task, 0, 'int');
		if (empty($cid))
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{
			// Get the model.
			$model = $this->getModel();

			// Make sure the item ids are integers
			$cid = ArrayHelper::toInteger($cid);

			// Publish the items.
			try
			{
				$model->publish($cid, $value);
				$errors = $model->getErrors();
				$ntext = null;

					if ($errors)
					{
						\JFactory::getApplication()->enqueueMessage(\JText::plural($this->text_prefix . '_N_ITEMS_FAILED_RESET', count($cid)), 'error');
					}
					else
					{
						$ntext = $this->text_prefix . '_N_ITEMS_'.$task;
					}
	
				if ($ntext !== null)
				{
					$this->setMessage(\JText::plural($ntext, count($cid)));
				}
			}
			catch (\Exception $e)
			{
				$this->setMessage($e->getMessage(), 'error');
			}
		}


		$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item. '&layout=edit&id='.$id, false , false));
	}


	


}
