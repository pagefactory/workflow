<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * The article controller
 *
 * @since  1.6
 */
class SixeWorkFlowControllerMessage extends JControllerForm
{
	/**
	 * Class constructor.
	 *
	 * @param   array  $config  A named array of configuration variables.
	 *
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

	}


	public function delete()
	{
		$return=false;
		$app=JFactory::getApplication();
		$id=$app->input->getInt('id',0);

		$db=JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete($query->qn('#__workflow_messages'))
				->where('id='.$id);
			$db->setQuery($query);
		$return=(boolean)$db->execute();
			if($return)
			{
				$this->setMessage(JText::_('COM_SIXEWORKFLOW_MESSAGES_DELETE_SUCCESS'));
			}
			else
			{
				$this->setMessage(JText::_('COM_SIXEWORKFLOW_MESSAGES_DELETE_ERROR'), 'error');
			}
		




		$this->setRedirect(JRoute::_('index.php?option=com_sixeworkflow&view=messages', false));


	}

}
