<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
use Joomla\Registry\Registry;

class com_sixeworkflowInstallerScript
{

public function __construct()
	{

		$lang = JFactory::getLanguage();
		$lang->load('com_sixeworkflow', JPATH_ADMINISTRATOR, null, false, true);


	}

  public function postflight($action, $installer)
    {
        if ($action === 'install')
		{
			$lang = JFactory::getLanguage();
			$lang->load('com_sixeworkflow', JPATH_ADMINISTRATOR, null, false, true);
			$this->CreateMenu();
		}
        
        return true;
    }


 	private function CreateMenu()
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)
			->select('extension_id')
			->from('#__extensions')
			->where('type=' .  $db->q('component'))
			->where('element='. $db->q('com_sixeworkflow'));
		$db->setQuery($query);
		$sixeworkflow_id = (int)$db->loadResult();

		if ($sixeworkflow_id > 0) {
			$query = $db->getQuery(true)
				->clear()
				->update('#__menu')
				->set('component_id='. (int)$sixeworkflow_id)
				->where(array('type='. $db->q('component'),'link LIKE '.$db->q('%option=com_sixeworkflow%')));
			$db->setQuery($query)->execute();
			$params='{"email_submit":"1","email_submit_body":"{from_name}\u63d0\u4ea4\u4e86{title}\uff0c\u8bf7\u60a8\u767b\u5f55\u7f51\u7ad9\u8fdb\u884c\u5ba1\u6838\u3002","email_reject":"1","email_reject_body":"{to_name}\u4f60\u597d: \u60a8\u63d0\u4ea4\u7684{title}\u88ab\u9a73\u56de\uff0c\u8bf7\u767b\u5f55\u7f51\u7ad9\u8fdb\u884c\u67e5\u770b\u3002 \u9a73\u56de\u5185\u5bb9\u662f\uff1a{message}\u3002","show_checked_by":"1"}';
				$query = $db->getQuery(true)
				->clear()
				->update('#__extensions')
				->set($db->quoteName('params') . ' = ' . $db->quote($params))
				->where('extension_id='.$sixeworkflow_id);
				$db->setQuery($query)->execute();
			if ($db->getErrorNum()) {
				throw new Exception ($db->getErrorMsg(), $db->getErrorNum());
			}

		}
		$query = $db->getQuery(true)
			->select('extension_id')
			->from('#__extensions')
			->where('type=' .  $db->q('component'))
			->where('element='. $db->q('com_users'));
		$db->setQuery($query);
		$users_id = (int)$db->loadResult();

		$menus = array(
			'new' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_FORM_VIEW_DEFAULT_TITLE'),
				'alias' => 'article-new',
				'link' => 'index.php?option=com_sixeworkflow&view=articleform&layout=edit',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'edits' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_EDITS_TITLE'),
				'alias' => 'article-edits',
				'link' => 'index.php?option=com_sixeworkflow&view=edits',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'checks' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_CHECKS_TITLE'),
				'alias' => 'article-checks',
				'link' => 'index.php?option=com_sixeworkflow&view=checks',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'checking' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_CHECKING_TITLE'),
				'alias' => 'article-checking',
				'link' => 'index.php?option=com_sixeworkflow&view=checking',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'rejects' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_REJECTS_TITLE'),
				'alias' => 'article-rejects',
				'link' => 'index.php?option=com_sixeworkflow&view=rejects',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),

			'completes' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_COMPLETE_TITLE'),
				'alias' => 'article-completes',
				'link' => 'index.php?option=com_sixeworkflow&view=completes',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'messages' => array(
				'name' => JText::_('COM_SIXEWORKFLOW_VIEW_MESSAGES_TITLE'),
				'alias' => 'messages',
				'link' => 'index.php?option=com_sixeworkflow&view=messages',
				'access' => 1,
				'component_id' => $sixeworkflow_id,
				'params' => array()
			),
			'logout' => array(
				'name' => JText::_('COM_USER_LOGOUT_VIEW_DEFAULT_TITLE'),
				'alias' => 'logout',
				'link' => 'index.php?option=com_users&view=login&layout=logout&task=user.menulogout',
				'access' => 1,
				'component_id' => $users_id,
				'params' => array('menu_show'=>0)
			),

		);


		$this->SaveMenu($menus);

	}
	private function SaveMenu($menus)
	{
		
		$menuTypeData = array(
				'menutype' => 'sixeworkflowmenu',
				'title' => JText::_('Sixe WorkFlow Menu'),
				'description' => JText::_('Sixe WorkFlow Menu')
			);
			$registry = new JRegistry($menuTypeData);
			$menuTypeData = $registry->toObject();

			$menuTypeTable = JTable::getInstance('MenuType');
			if (!$menuTypeTable->bind($menuTypeData) || !$menuTypeTable->check()) {
				// Menu already exists, do nothing
				return true;
			}

		// Create menu Type
			if (!$menuTypeTable->store()) {
				throw new Exception ($menuTypeTable->getError());
			}

			// Create menus
			foreach ($menus as $menuitem) {
				$params = new JRegistry();
				$params->loadArray($menuitem['params']);

				$data = array(
					'menutype' => 'sixeworkflowmenu',
					'title' => $menuitem ['name'],
					'alias' => $menuitem ['alias'],
					'link' => $menuitem ['link'],
					'type' => 'component',
					'published' => 1,
					'parent_id' => 1,
					'component_id' => $menuitem ['component_id'],
					'access' => $menuitem ['access'],
					'params' => (string)$params,
					'home' => 0,
					'language' => '*',
					'client_id' => 0
				);

				$menuTable = JTable::getInstance('Menu');
				$menuTable->load(array(
					'menutype' => 'sixeworkflowmenu',
					'link' => $menuitem ['link']
				));

				$menuTable->setLocation(1, 'last-child');

				if (!$menuTable->bind($data) || !$menuTable->check() || !$menuTable->store()) {
					throw new Exception ($menuTable->getError());
				}
			}

}

}