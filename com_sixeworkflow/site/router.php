<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

/**
 * Routing class of com_content
 *
 * @since  3.3
 */
class SixeWorkFlowRouter extends JComponentRouterView
{
	protected $noIDs = false;

	/**
	 * Content Component router constructor
	 *
	 * @param   JApplicationCms  $app   The application object
	 * @param   JMenu            $menu  The menu object to work with
	 */
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getParams('com_content');
		$completes = new JComponentRouterViewconfiguration('completes');
		$this->registerView($completes);
		$edits = new JComponentRouterViewconfiguration('edits');
		$this->registerView($edits);
		$rejects = new JComponentRouterViewconfiguration('rejects');
		$this->registerView($rejects);
		$messages = new JComponentRouterViewconfiguration('messages');
		$this->registerView($messages);
		$message = new JComponentRouterViewconfiguration('message');
		$message->setKey('id');
		$message->setParent($messages,'layout');
		$this->registerView($message);

		$article = new JComponentRouterViewconfiguration('article');
		$article->setKey('id');
		$this->registerView($article);
		$submits = new JComponentRouterViewconfiguration('submits');
		$this->registerView($submits);
		$form = new JComponentRouterViewconfiguration('articleform');
		$form->setKey('a_id');
		$this->registerView($form);

		parent::__construct($app, $menu);

		$this->attachRule(new JComponentRouterRulesMenu($this));
		
			JLoader::register('SixeWorkFlowRouterRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			$this->attachRule(new SixeWorkFlowRouterRulesLegacy($this));
		
	}





public function getArticleFormSegment($id, $query)
	{
		//return $this->getArticleSegment($id, $query);
		
		return array((int) $id => $id);
	}





/**
 * Content router functions
 *
 * These functions are proxys for the new router interface
 * for old SEF extensions.
 *
 * @param   array  &$query  An array of URL arguments
 *
 * @return  array  The URL arguments to use to assemble the subsequent URL.
 *
 * @deprecated  4.0  Use Class based routers instead
 */
function SixeWorkFlowBuildRoute(&$query)
{
	$app = JFactory::getApplication();
	$router = new WorkFlowRouter($app, $app->getMenu());

	return $router->build($query);
}

/**
 * Parse the segments of a URL.
 *
 * This function is a proxy for the new router interface
 * for old SEF extensions.
 *
 * @param   array  $segments  The segments of the URL to parse.
 *
 * @return  array  The URL attributes to be used by the application.
 *
 * @since   3.3
 * @deprecated  4.0  Use Class based routers instead
 */
function SixeWorkFlowParseRoute($segments)
{
	$app = JFactory::getApplication();
	$router = new WorkFlowRouter($app, $app->getMenu());

	return $router->parse($segments);
}

}