<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
// No direct access
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$types=array('1'=>'COM_SIXEWORKFLOW_MESSAGES_TYPE_CHECK','2'=>'COM_SIXEWORKFLOW_MESSAGES_TYPE_REJECT');

$js="
  function cheage_filter(valu, element, task, form)
  {
  if ( typeof form  === 'undefined' ) {
      form = document.getElementById( 'adminForm' );
    }

    filter=document.getElementById(element);
    filter.value = valu;

    Joomla.submitform( task, form );
  }
  
  ";
 JText::script('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST');
 $doc=JFactory::getDocument();
 $doc->addScriptDeclaration($js);
$doc->addStyleSheet(JUri::root(true) . '/components/com_sixeworkflow/assets/css/sixeworkflow.css');
?>
<div class="workflow-content">

<div class="workflow-menu">

<?php echo JLayoutHelper::render('workflow.menu.menu', $this->menu); ?>
</div>
<div class="message-list workflow-main">
  <?php if(!$this->items) :?>
      <p><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGES_NO_ITEM');?></p>
  
<?php else :?>
  <div class="toolbar">
  <button onclick="if (document.adminForm.boxchecked.value == 0) { alert(Joomla.JText._('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST')); } else { Joomla.submitbutton('messages.read'); }" class="btn-small button-publish">
  <span class="icon-publish" aria-hidden="true"></span>
  已读</button>
  <button onclick="if (document.adminForm.boxchecked.value == 0) { alert(Joomla.JText._('JLIB_HTML_PLEASE_MAKE_A_SELECTION_FROM_THE_LIST')); } else { Joomla.submitbutton('messages.delete'); }" class=" btn-small button-unpublish">
  <span class="icon-unpublish" aria-hidden="true"></span>
  删除</button>
</div>
<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm" class="form-inline">

  <table class="table table-striped" >
        <thead>
          <tr>

            <th width="1%" class="center">
              <?php echo JHtml::_('grid.checkall'); ?>
            </th>
             <th style="min-width:100px" class="nowrap">
              <?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_TITLE', 'title', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="nowrap center">
              <?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_TYPE', 'a.type', $listDirn, $listOrder); ?>
            </th>


            <th width="10%" class="nowrap hidden-phone">
              <?php echo JHtml::_('grid.sort',  'COM_SIXEWORKFLOW_HEADING_FROMUSER', 'username', $listDirn, $listOrder); ?>
            </th>
            <th width="15%" class="nowrap hidden-phone">
              <?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_DATE_CREATED', 'a.created', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class="nowrap hidden-phone">
              <?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_READ', 'a.is_read', $listDirn, $listOrder); ?>
            </th>
            <th width="10%"><?php echo JText::_('COM_SIXEWORKFLOW_HEADING_ACTION');?></th>



          </tr>
        </thead>

        <tbody>
        <?php foreach ($this->items as $i => $item) :?>
          <tr class="row<?php echo $i % 2; ?>" >

            <td class="center">
              <?php echo JHtml::_('grid.id', $i, $item->id); ?>
            </td>
            <td>
              <a href="<?php echo JRoute::_('index.php?option=com_sixeworkflow&view=message&id='.$item->id,false);?>" class="<?php echo $item->is_read ? 'read' : 'no-read';?>"><?php echo $item->title ;?></a>
            </td>
            <td class="center">
              <?php echo JText::_($types[$item->type]) ;?>
            </td>

            <td><?php echo $item->username;?></td>
            <td class="nowrap small hidden-phone">
              <?php
              $date = $item->created;
              echo $date > 0 ? JHtml::_('date', $date, JText::_('DATE_FORMAT_LC4')) : '-';
              ?>
            </td>
            <td class="hidden-phone center">
              <?php echo $item->is_read ? JText::_('JYES') : JText::_('JNO') ;?>
            </td>
            
            <td class="hidden-phone">
              <a onclick="return confirm('您确定要删除该消息吗？');" href="<?php echo JRoute::_('index.php?option=com_sixeworkflow&task=message.delete&id='.$item->id.'&return=' . base64_encode(JUri::getInstance()),false);?>">删除</a>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <input type="hidden" name="filter_order" value="" />
      <input type="hidden" name="filter_order_Dir" value="" />
      <input type="hidden" name="boxchecked" value="0" />
      <input type="hidden" name="task"  value="" />
      <?php echo JHtml::_('form.token'); ?>
</form>


  <?php if (!empty($this->items)) : ?>
  <?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
    <div class="pagination">

      <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
  <?php endif; ?>
<?php endif; ?>
<?php endif;?>
</div>



</div>