<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
// No direct access
defined('_JEXEC') or die;

$contentUrl = SixeWorkFlowHelperRoute::getArticleEditRoute($this->item->content_id, $this->item->catid, $this->item->language);
$uri  = JUri::getInstance();
$url        = $contentUrl . '&task=article.edit&a_id=' . $this->item->content_id . '&return=' . base64_encode($uri);
$output =JHtml::_('link', JRoute::_($url), $this->item->title);
if($this->item->message=='')
{
    $this->item->message=JText::_('COM_SIXEWORKFLOW_FORM_NOMESSAGE');
}
$doc=JFactory::getDocument();

$doc->addStyleSheet(JUri::root(true) . '/components/com_sixeworkflow/assets/css/sixeworkflow.css');
?>
<div class="workflow-content">
<div class="workflow-menu">

<?php echo JLayoutHelper::render('workflow.menu.menu', $this->menu); ?>
</div>

<div class="message workflow-main">
<table class="table table-striped table-bordered table-hover ">
<thead><tr><th colspan="2"><?php echo $this->item->title;?></th></tr></thead>
<tbody>
	<tr>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_FROM_NAME');?></td>
	 <td><?php echo $this->item->from_name;?></td>
	</tr>
	<tr>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_CREATED');?></td>
	 <td><?php echo JHtml::_('date',$this->item->created,JText::_('Y-m-d H:i:s')) ;?></td>
	</tr>
	<tr>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_TYPE');?></td>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_TYPE_'.$this->item->type);?></td>
	</tr>
	<tr>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_LINK');?></td>
	 <td><?php echo $output;?></td>
	</tr>
	<tr>
	 <td><?php echo JText::_('COM_SIXEWORKFLOW_MESSAGE_MESSAGE');?></td>
	 <td><?php echo $this->item->message;?></td>
	</tr>
</tbody>
</table>
</div>

</div>



