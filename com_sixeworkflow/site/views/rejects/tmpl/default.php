<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Multilanguage;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Create some shortcuts.
$params     = &$this->item->params;
$n          = count($this->items);
$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$langFilter = false;

// Tags filtering based on language filter 
if (($this->params->get('filter_field') === 'tag') && (Multilanguage::isEnabled()))
{ 
	$tagfilter = ComponentHelper::getParams('com_tags')->get('tag_list_language_filter');

	switch ($tagfilter)
	{
		case 'current_language' :
			$langFilter = JFactory::getApplication()->getLanguage()->getTag();
			break;

		case 'all' :
			$langFilter = false;
			break;

		default :
			$langFilter = $tagfilter;
	}
}

// Check for at least one editable article
$isEditable = false;

if (!empty($this->items))
{
	foreach ($this->items as $article)
	{
		if ($article->params->get('access-edit'))
		{
			$isEditable = true;
			break;
		}
	}
}



// For B/C we also add the css classes inline. This will be removed in 4.0.
$doc=JFactory::getDocument();
$doc->addStyleDeclaration('
.hide { display: none; }
.table-noheader { border-collapse: collapse; }
.table-noheader thead { display: none; }
');
$doc->addStyleSheet(JUri::root(true) . '/components/com_sixeworkflow/assets/css/sixeworkflow.css');

$tableClass = $this->params->get('show_headings') != 1 ? ' table-noheader' : '';
?>

<div class="workflow-content">

<div class="workflow-menu">

<?php echo JLayoutHelper::render('workflow.menu.menu', $this->menu); ?>
</div>

<div class="workflow-main">
<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm" class="form-inline">
<?php if ($this->params->get('filter_field') !== 'hide' || $this->params->get('show_pagination_limit')) : ?>
	<fieldset class="filters btn-toolbar clearfix">
		



		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />
		<input type="hidden" name="task" value="" />
	</fieldset>



<?php endif; ?>

<?php if (empty($this->items)) : ?>
	<?php if ($this->params->get('show_no_articles', 1)) : ?>
		<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
	<?php endif; ?>
<?php else : ?>
	<table class="category table table-striped table-bordered table-hover<?php echo $tableClass; ?>">
		
		<thead>
			<tr>
				<th scope="col" id="header_id" class="center">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder, null, 'asc', '', 'adminForm'); ?>
				</th>
				<th scope="col" id="header_title" class="center">
					<?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder, null, 'asc', '', 'adminForm'); ?>
				</th>
				<th scope="col" id="header_created" class="center">
						
							<?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_DATE_CREATED', 'a.created', $listDirn, $listOrder); ?>

				</th>
				
					<th scope="col" id="header_author" class="center">
						<?php echo JHtml::_('grid.sort', 'JAUTHOR', 'author', $listDirn, $listOrder); ?>
					</th>
					<th scope="col" id="header_rejected_name" class="center">
						<?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_REJECTED_NAME', 'rejected_name', $listDirn, $listOrder); ?>
					</th>

				
					<th scope="col" id="header_checked" class="center">
						
							<?php echo JHtml::_('grid.sort', 'COM_SIXEWORKFLOW_HEADING_DATE_REJECTED', 'a.rejected', $listDirn, $listOrder); ?>

					</th>
				
				
				
				<?php if ($isEditable) : ?>
					<th scope="col" id="header_action" class="center"><?php echo JText::_('COM_SIXEWORKFLOW_HEADING_ACTION'); ?></th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->items as $i => $article) : ?>
			<?php if ($this->items[$i]->state == 0) : ?>
				<tr class="system-unpublished cat-list-row<?php echo $i % 2; ?>">
			<?php else : ?>
				<tr class="cat-list-row<?php echo $i % 2; ?>" >
			<?php endif; ?>
			<td headers="header_id" class="list-id center">
				<?php echo $article->id;?>
			</td>
			<td headers="header_title" class="list-title center">
					<a href="<?php echo JRoute::_(SixeWorkFlowHelperRoute::getArticleRoute($article->slug, $article->catid, $article->language)); ?>">
						<?php echo $this->escape($article->title); ?>
					</a>


				
			</td>
				<td headers="header_created" class="list-date small center">
					<?php
					echo JHtml::_(
						'date', $article->created,
						$this->escape($this->params->get('date_format', JText::_('Y-m-d')))
					); ?>
				</td>
			

				<td headers="header_author" class="list-author center">
					<?php if (!empty($article->author) || !empty($article->created_by_alias)) : ?>
						<?php $author = $article->author ?>
						<?php $author = $article->created_by_alias ?: $author; ?>

							<?php echo $author; ?>
						
					<?php endif; ?>
				</td>
				<td headers="header_rejected_name" class="list-checked_name center">
					<?php echo $article->rejected_name; ?>
				</td>

				<td headers="header_rejected" class="list-date small center">
					<?php
					if($article->rejected=='0000-00-00 00:00:00')
						{
							$article->rejected='';
						}
						else
						{
							echo JHtml::_(
						'date', $article->rejected,
						$this->escape($this->params->get('date_format', JText::_('Y-m-d')))
					);
						}
					 ?>
				</td>
			
			<?php if ($isEditable) : ?>
				<td headers="header_action" class="list-edit center">
					<?php if ($article->params->get('access-edit')) : ?>
						<?php echo JHtml::_('action.edit', $article, $params); ?>
					<?php endif; ?>
				</td>
			<?php endif; ?>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>





<?php // Add pagination links ?>
<?php if (!empty($this->items)) : ?>
	<?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
		<div class="pagination">

			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<p class="counter pull-right">
					<?php echo $this->pagination->getPagesCounter(); ?>
				</p>
			<?php endif; ?>
			<?php echo $this->pagination->total;?>
			<?php echo $this->pagination->getPagesLinks(); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
<?php echo JHtml::_('form.token'); ?>
</form>
</div>

</div>