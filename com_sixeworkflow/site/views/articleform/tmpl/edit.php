<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

JHtml::_('behavior.tabstate');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0));
JHtml::_('formbehavior.chosen', 'select');
$this->tab_name = 'com-workflow-form';
$this->ignore_fieldsets = array('image-intro', 'image-full', 'jmetadata', 'item_associations');

// Create shortcut to parameters.
$params = $this->state->get('params');
$isNew=($this->item->id==0);
// This checks if the editor config options have ever been saved. If they haven't they will fall back to the original settings.
if($isNew)
{
	$this->ignore_fieldsets[]='workflow';
	$this->ignore_fieldsets[]='messages';
}
$editoroptions = isset($params->show_publishing_options);

if (!$editoroptions)
{
	$params->show_urls_images_frontend = '0';
}
$doc=JFactory::getDocument();
$doc->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'article.cancel' || document.formvalidator.isValid(document.getElementById('adminForm')))
		{
			" . $this->form->getField('articletext')->save() . "
			Joomla.submitform(task);
		}
	}
");


$doc->addStyleSheet(JUri::root(true) . '/components/com_sixeworkflow/assets/css/sixeworkflow.css');

// add by adan20191209

// 是否允许审核者编辑
$comParams = JComponentHelper::getParams('com_sixeworkflow');
$permit_edit_by_checker = $comParams->get('permit_edit_by_checker', 0);

// 当前登录用户id
$user = JFactory::getUser();
$isAuthor = !empty($user->id) && !empty($this->item->created_by) && $user->id == $this->item->created_by;
?>

<div class="workflow-content">


<div class="workflow-menu">

<?php echo JLayoutHelper::render('workflow.menu.menu', $this->menu); ?>
</div>




<div class="workflow-main edit item-page<?php echo $this->pageclass_sfx; ?>">
	<?php if ($params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>
	
	<?php if($this->item->message !='') :?>
	<div class="alert alert-error"><h4>驳回原因：</h4>
		<?php echo $this->item->message;?>
	</div>
		
	
	<?php endif;?>
	<form action="<?php echo JRoute::_('index.php?option=com_sixeworkflow&a_id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">
		<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', $this->tab_name, array('active' => 'editor')); ?>

			<?php echo JHtml::_('bootstrap.addTab', $this->tab_name, 'editor', JText::_('COM_CONTENT_ARTICLE_CONTENT')); ?>
				<?php echo $this->form->renderField('title'); ?>

				<?php if (is_null($this->item->id)) : ?>
					<?php echo $this->form->renderField('alias'); ?>
				<?php endif; ?>
				<?php echo $this->form->renderField('catid'); ?>
				<?php echo $this->form->renderField('language'); ?>
				<?php echo $this->form->renderField('tags'); ?>
				<?php echo $this->form->getInput('articletext'); ?>
				
				<?php if ($this->captchaEnabled) : ?>
					<?php echo $this->form->renderField('captcha'); ?>
				<?php endif; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>

			<?php if ($params->get('show_urls_images_frontend')) : ?>
			<?php echo JHtml::_('bootstrap.addTab', $this->tab_name, 'images', JText::_('COM_CONTENT_IMAGES_AND_URLS')); ?>
				<?php echo $this->form->renderField('image_intro', 'images'); ?>
				<?php echo $this->form->renderField('image_intro_alt', 'images'); ?>
				<?php echo $this->form->renderField('image_intro_caption', 'images'); ?>
				<?php echo $this->form->renderField('float_intro', 'images'); ?>
				<?php echo $this->form->renderField('image_fulltext', 'images'); ?>
				<?php echo $this->form->renderField('image_fulltext_alt', 'images'); ?>
				<?php echo $this->form->renderField('image_fulltext_caption', 'images'); ?>
				<?php echo $this->form->renderField('float_fulltext', 'images'); ?>
				<?php echo $this->form->renderField('urla', 'urls'); ?>
				<?php echo $this->form->renderField('urlatext', 'urls'); ?>
				<div class="control-group">
					<div class="controls">
						<?php echo $this->form->getInput('targeta', 'urls'); ?>
					</div>
				</div>
				<?php echo $this->form->renderField('urlb', 'urls'); ?>
				<?php echo $this->form->renderField('urlbtext', 'urls'); ?>
				<div class="control-group">
					<div class="controls">
						<?php echo $this->form->getInput('targetb', 'urls'); ?>
					</div>
				</div>
				<?php echo $this->form->renderField('urlc', 'urls'); ?>
				<?php echo $this->form->renderField('urlctext', 'urls'); ?>
				<div class="control-group">
					<div class="controls">
						<?php echo $this->form->getInput('targetc', 'urls'); ?>
					</div>
				</div>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php echo JLayoutHelper::render('workflow.edit.params', $this); ?>

			<?php echo JHtml::_('bootstrap.addTab', $this->tab_name, 'publishing', JText::_('COM_CONTENT_PUBLISHING')); ?>
				
				
				<?php echo $this->form->renderField('note'); ?>
				<?php if ($params->get('save_history', 0)) : ?>
					<?php echo $this->form->renderField('version_note'); ?>
				<?php endif; ?>
				<?php if ($params->get('show_publishing_options', 1) == 1) : ?>
					<?php echo $this->form->renderField('created_by_alias'); ?>
				<?php endif; ?>
				
					<?php echo $this->form->renderField('state'); ?>
					<?php echo $this->form->renderField('featured'); ?>
					<?php if ($params->get('show_publishing_options', 1) == 1) : ?>
						<?php echo $this->form->renderField('publish_up'); ?>
						<?php echo $this->form->renderField('publish_down'); ?>
					<?php endif; ?>
				
				<?php echo $this->form->renderField('access'); ?>
				<?php if (is_null($this->item->id)) : ?>
					<div class="control-group">
						<div class="control-label">
						</div>
						<div class="controls">
							<?php echo JText::_('COM_CONTENT_ORDERING'); ?>
						</div>
					</div>
				<?php endif; ?>
			<?php echo JHtml::_('bootstrap.endTab'); ?>



			<?php if ($params->get('show_publishing_options', 1) == 1) : ?>	
				<?php echo JHtml::_('bootstrap.addTab', $this->tab_name, 'metadata', JText::_('COM_CONTENT_METADATA')); ?>
					<?php echo $this->form->renderField('metadesc'); ?>
					<?php echo $this->form->renderField('metakey'); ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php endif; ?>

			<?php echo JHtml::_('bootstrap.endTabSet'); ?>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
		<div class="btn-toolbar">
			<?php if($isNew || (($permit_edit_by_checker || $isAuthor) && in_array($this->item->flow,$this->userflow))) :?>
			<div class="btn-group">
				<button type="button" class="btn btn-primary" onclick="Joomla.submitbutton('article.save')">
					<span class="icon-ok"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_SAVE') ?>
				</button>
			</div>
			<?php endif;?>
			<div class="btn-group">
				<button type="button" class="btn btn-danger" onclick="Joomla.submitbutton('article.cancel')">
					<span class="icon-cancel"></span><?php echo JText::_('JCANCEL') ?>
				</button>
			</div>
			<?php if ($params->get('save_history', 0) && $this->item->id && false) : ?>
			<div class="btn-group">
				<?php echo $this->form->getInput('contenthistory'); ?>
			</div>
			<?php endif; ?>

			<?php if($isNew || (($permit_edit_by_checker || $isAuthor) && in_array($this->item->flow,$this->userflow))) :?>
				<div class="btn-group">
					<button type="button" class="btn btn-success" onclick="Joomla.submitbutton('article.save2view')">
					<span class="icon-eye"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_SAVE2VIEW') ?>
					</button>

				</div>
		<?php endif;?>
		<?php if(!$isNew ) :?>
			<div class="btn-group">
				<button  type="button" id="preview-button"  onclick="window.open('<?php echo JRoute::_(SixeWorkFlowHelperRoute::getArticleRoute($this->item->id, $this->item->catid, $this->item->language)); ?>')" class="btn"  ><span class="icon-eye"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_PREVIEW');?></button>
			</div>
			<?php endif;?>
		<?php if( $isNew || in_array($this->item->flow,$this->userflow)) :?>
			
			<?php if(($this->item->flow <= $this->item->max_flow) || $isNew ) :?>
				<div class="btn-group">
					<button type="button" class="btn btn-warning" onclick="Joomla.submitbutton('article.submit')">
						<span class="icon-share"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_SUBMIT') ?>
					</button>
				</div>
			<?php endif;?>

			<?php if(($this->item->flow > $this->item->start_flow) && !$isNew) :?>

			<div class="btn-group">
					<button type="button" class="btn btn-info"  onclick="openreject(); return false;" class="btn btn-small">
						<span class="icon-reply"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_REJECT') ?>
					</button>
				</div>
			<?php endif;?>

		<?php if($this->item->flow == $this->item->max_flow && !$isNew && false) :?>
				<div class="btn-group">
					<button type="button" class="btn" onclick="Joomla.submitbutton('article.complete')">
						<span class="icon-ok"></span><?php echo JText::_('COM_SIXEWORKFLOW_FORM_CONTENT_TOOLBAR_COMPLETE') ?>
					</button>
				</div>

				

			<?php endif;?>
		

		<?php endif;?>

		</div>

	<div id="collapseModal" >
		<div class="reject-main">
			<div class="reject-header">
				<button type="button" class="close-reject" onclick="closereject()" aria-label="关闭">
				<span aria-hidden="true">×</span>
				</button>
				<h3>驳回文章</h3>
			</div>
		<div class="reject-body">
	
		<div class="container-fluid">
		<div class="row-fluid">
			<div class="control-group">
			<div class="control-label">
				<label id="jform_message-lbl" for="jform_message">驳回的原因</label>
			</div>
			<div class="controls"><textarea name="jform[message]" id="jform_message" cols="20" rows="5" class="span8"></textarea></div>
			</div>
		</div>

		</div>
		</div>
<div class="reject-footer">
	<button type="button" class="btn" onclick="closereject()" >	取消</button>
	<button type="button" class="btn btn-success" onclick="Joomla.submitbutton('article.reject');">
	驳回文章</button>
</div>
</div>
</div>

<script>

	function openreject()
	{
		
		jQuery( '#collapseModal' ).css('display','block');
 	 setTimeout(function(){jQuery('#collapseModal').addClass('reject-open');},100);
	}
	
	function closereject()
	{
		jQuery( '#collapseModal' ).removeClass('reject-open');

 		setTimeout(function(){jQuery('#collapseModal').css('display','none');},1000);
	}

</script>


	</form>
</div>

</div>
