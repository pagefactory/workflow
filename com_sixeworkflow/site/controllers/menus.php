<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Content article class.
 *
 * @since  1.6.0
 */
class SixeWorkFlowControllerMenus extends JControllerForm
{
	public function CheckNew()
	{
	$app=JFactory::getApplication();	
	$counts=$this->GetArticleTotal();
	$message_total=$this->GetMessageTotal();
	$view_count=array('edits'=>$counts[3],'checks'=>$counts[4],'rejects'=>$counts[5],'messages'=>$message_total);
	$total=0;
	foreach($view_count as $key=>$c)
	{

			$total+=$c;
	
		
	}
		if ($total > 0 )
		{
			// Output a JSON object
			$result=new stdclass;
			$result->count=$total;
			$result->views=$view_count;
			echo json_encode($result);
		}
		else
		{
		$noresults=array();
		echo json_encode($noresults);
		}
		$app->close();
	}



	function GetArticleTotal()
	{
		$return=array();
		$user=JFactory::getUser();
		$groups=implode(',',$user->groups);
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
 		$subquery='(SELECT COUNT(*) FROM #__workflow_users WHERE workflow_id=wc.workflow_id AND flow_id=c.flow AND 
 		((type=1 AND user_id IN('.$groups.')) OR (type=2 AND user_id='.$user->id.'))) > 0 ';
		$query
			->select('DISTINCT c.state,COUNT(*) AS count');
		$query->from('#__workflow_contents AS c')
			->join('INNER','#__workflow_categories AS wc ON wc.category_id=c.catid')
			->where('((c.created_by='.$user->id.' AND c.flow=c.start_flow) OR (c.flow > c.start_flow AND '.$subquery.' ))')
			->group('c.state');
			$db->setQuery($query);
	 $groups= $db->loadObjectList();
		foreach($groups as $g)
		{
			$return[$g->state]=$g->count;
		}
	return	$return;
	}

	

	function GetMessageTotal()
	{
		$user=JFactory::getUser();
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(*)')
	      ->from('#__workflow_messages')
	      ->where('to_user='.$user->id)
	      ->where('is_read=0');
		$db->setQuery($query);
		return $db->loadResult();  
	}


}
