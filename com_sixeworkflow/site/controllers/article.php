<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

/**
 * Content article class.
 *
 * @since  1.6.0
 */
class SixeWorkFlowControllerArticle extends JControllerForm
{
	/**
	 * The URL view item variable.
	 *
	 * @var    string
	 * @since  1.6
	 */
	protected $view_item = 'articleform';

	/**
	 * The URL view list variable.
	 *
	 * @var    string
	 * @since  1.6
	 */
	protected $view_list = 'edits';

	/**
	 * The URL edit variable.
	 *
	 * @var    string
	 * @since  3.2
	 */
	protected $urlVar = 'a.id';

	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask('save2view', 'save');
		$this->registerTask('submit', 'save');
		$this->registerTask('reject', 'save');
		$this->registerTask('complete', 'save');
	}


	/**
	 * Method to add a new record.
	 *
	 * @return  mixed  True if the record can be added, an error object if not.
	 *
	 * @since   1.6
	 */
	public function add()
	{
		if (!parent::add()) {
			// Redirect to the return page.
			$this->setRedirect($this->getReturnPage());

			return;
		}

		// Redirect to the edit screen.
		$this->setRedirect(
			JRoute::_(
				'index.php?option=' . $this->option . '&view=' . $this->view_item . '&a_id=0'
				. $this->getRedirectToItemAppend(), false
			)
		);

		return true;
	}

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array $data An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array())
	{
		$model = $this->getModel();
		return $model->getIs_Add();
	}

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param   array $data An array of input data.
	 * @param   string $key The name of the key for the primary key; default is id.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int)isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		$model = $this->getModel();
		// Zero record (id:0), return component edit permission by calling parent controller method
		if (!$recordId) {
			return parent::allowEdit($data, $key);
		}

		// Check edit on the record asset (explicit or inherited)
		if ($model->getEditAccess($recordId)) {
			return true;
		}

		// Check edit own on the record asset (explicit or inherited)
		if ($user->authorise('core.edit.own', 'com_content.article.' . $recordId)) {
			// Existing record already has an owner, get it
			$record = $this->getModel()->getItem($recordId);

			if (empty($record)) {
				return false;
			}

			// Grant if current user is owner of the record
			return $user->get('id') == $record->created_by;
		}

		return false;
	}

	/**
	 * Method to cancel an edit.
	 *
	 * @param   string $key The name of the primary key of the URL variable.
	 *
	 * @return  boolean  True if access level checks pass, false otherwise.
	 *
	 * @since   1.6
	 */
	public function cancel($key = 'a_id')
	{
		parent::cancel($key);

		$app = JFactory::getApplication();

		// Load the parameters.
		$params = $app->getParams();

		$customCancelRedir = (bool)$params->get('custom_cancel_redirect');

		if ($customCancelRedir) {
			$cancelMenuitemId = (int)$params->get('cancel_redirect_menuitem');

			if ($cancelMenuitemId > 0) {
				$item = $app->getMenu()->getItem($cancelMenuitemId);
				$lang = '';

				if (JLanguageMultilang::isEnabled()) {
					$lang = !is_null($item) && $item->language != '*' ? '&lang=' . $item->language : '';
				}

				// Redirect to the user specified return page.
				$redirlink = $item->link . $lang . '&Itemid=' . $cancelMenuitemId;
			} else {
				// Redirect to the same article submission form (clean form).
				$redirlink = $app->getMenu()->getActive()->link . '&Itemid=' . $app->getMenu()->getActive()->id;
			}
		} else {
			$menuitemId = (int)$params->get('redirect_menuitem');
			$lang = '';

			if ($menuitemId > 0) {
				$lang = '';
				$item = $app->getMenu()->getItem($menuitemId);

				if (JLanguageMultilang::isEnabled()) {
					$lang = !is_null($item) && $item->language != '*' ? '&lang=' . $item->language : '';
				}

				// Redirect to the general (redirect_menuitem) user specified return page.
				$redirlink = $item->link . $lang . '&Itemid=' . $menuitemId;
			} else {
				// Redirect to the return page.
				$redirlink = $this->getReturnPage();
			}
		}

		$this->setRedirect(JRoute::_($redirlink, false));
	}

	/**
	 * Method to edit an existing record.
	 *
	 * @param   string $key The name of the primary key of the URL variable.
	 * @param   string $urlVar The name of the URL variable if different from the primary key
	 * (sometimes required to avoid router collisions).
	 *
	 * @return  boolean  True if access level check and checkout passes, false otherwise.
	 *
	 * @since   1.6
	 */
	public function edit($key = null, $urlVar = 'a_id')
	{

		$id = $this->input->get('a_id');
		$model = $this->getModel();
		$return = $model->checkin($id);
		$result = parent::edit($key, $urlVar);

		if (!$result) {
			$this->setRedirect(JRoute::_($this->getReturnPage(), false));
		}

		return $result;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string $name The model name. Optional.
	 * @param   string $prefix The class prefix. Optional.
	 * @param   array $config Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.5
	 */
	public function getModel($name = 'Articleform', $prefix = '', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param   integer $recordId The primary key id for the item.
	 * @param   string $urlVar The name of the URL variable for the id.
	 *
	 * @return  string    The arguments to append to the redirect URL.
	 *
	 * @since   1.6
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'a_id')
	{
		// Need to override the parent method completely.
		$tmpl = $this->input->get('tmpl');

		$append = '';

		// Setup redirect info.
		if ($tmpl) {
			$append .= '&tmpl=' . $tmpl;
		}

		// TODO This is a bandaid, not a long term solution.
		/**
		 * if ($layout)
		 * {
		 *    $append .= '&layout=' . $layout;
		 * }
		 */

		$append .= '&layout=edit';

		if ($recordId) {
			$append .= '&' . $urlVar . '=' . $recordId;
		}

		$itemId = $this->input->getInt('Itemid');
		//$return = $this->getReturnPage();
		$catId = $this->input->getInt('catid');

		if ($itemId) {
			$append .= '&Itemid=' . $itemId;
		}

		if ($catId) {
			$append .= '&catid=' . $catId;
		}
		$return = $this->input->get('return', null, 'base64');
		if ($return) {
			$append .= '&return=' . $return;
		}

		return $append;
	}

	/**
	 * Get the return URL.
	 *
	 * If a "return" variable has been passed in the request
	 *
	 * @return  string    The return URL.
	 *
	 * @since   1.6
	 */
	protected function getReturnPage()
	{
		$return = $this->input->get('return', null, 'base64');

		if (empty($return) || !JUri::isInternal(base64_decode($return))) {
			return JUri::root();
		} else {
			return base64_decode($return);
		}
	}


	public function save($key = null, $urlVar = 'a_id')
	{
		// Check for request forgeries.
		$this->checkToken();

		$app = \JFactory::getApplication();
		$user = JFactory::getUser();
		$model = $this->getModel();
		$table = $model->getTable();
		$data = $this->input->post->get('jform', array(), 'array');

		$checkin = property_exists($table, $table->getColumnAlias('checked_out'));
		$context = "$this->option.edit.$this->context";
		$task = $this->getTask();

		// Determine the name of the primary key for the data.
		if (empty($key)) {
			$key = $table->getKeyName();
		}

		// To avoid data collisions the urlVar may be different from the primary key.
		if (empty($urlVar)) {
			$urlVar = $key;
		}

		$recordId = $this->input->getInt($urlVar);

		// Populate the row id from the session.
		$data[$key] = $recordId;


		// Access check.
		if (!$this->allowSave($data, $key)) {

			$this->setError(\JText::_('JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend(), false
				)
			);

			return false;
		}

		// Validate the posted data.
		// Sometimes the form needs some posted data, such as for plugins and modules.
		$form = $model->getForm($data, false);

		if (!$form) {
			$app->enqueueMessage($model->getError(), 'error');

			return false;
		}

		// Send an object which can be modified through the plugin event
		$objData = (object)$data;
		$app->triggerEvent(
			'onContentNormaliseRequestData',
			array($this->option . '.' . $this->context, $objData, $form)
		);
		$data = (array)$objData;

		// Test whether the data is valid.
		$validData = $model->validate($form, $data);

		// Check for validation errors.
		if ($validData === false) {
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof \Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState($context . '.data', $data);

			// Redirect back to the edit screen.
			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}

		if (!isset($validData['tags'])) {
			$validData['tags'] = null;
		}

		// add by adan20191211
		$saveResult = false;
		switch ($task) {
			case 'submit':
				if ($recordId == 0) {
					if (!$model->save($validData)) {
						// Save the data in the session.
						$app->setUserState($context . '.data', $validData);

						// Redirect back to the edit screen.
						$this->setError(\JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
						$this->setMessage($this->getError(), 'error');

						$this->setRedirect(
							\JRoute::_(
								'index.php?option=' . $this->option . '&view=' . $this->view_item
								. $this->getRedirectToItemAppend($recordId, $urlVar), false
							)
						);

						return false;
					}

					$recordId = $model->getState($this->context . '.id');
					$newitem = $model->getFlowContent($recordId);
					$validData['flow'] = $newitem->flow;
					$validData['max_flow'] = $newitem->max_flow;
					$validData['start_flow'] = $newitem->start_flow;
					$validData['created_by'] = $newitem->created_by;
					$validData['id'] = $recordId;
				}

				if ((int)$validData['flow'] < (int)$validData['max_flow']) {
					$validData['flow_id'] = (int)$validData['flow'];
					$validData['flow'] = (int)$validData['flow'] + 1;
					$validData['flowstate'] = 4;
					$validData['message'] = '';
				} elseif ((int)$validData['flow'] == (int)$validData['max_flow']) {
					$validData['flow_id'] = (int)$validData['flow'];
					$validData['flowstate'] = 1;
					$validData['message'] = '';
				}

				$saveResult = $model->approval($validData);
				break;
			case 'reject':

				if ((int)$validData['flow'] > (int)$validData['start_flow']) {
					$validData['flow_id'] = (int)$validData['flow'];
					$validData['flow'] = (int)$validData['flow'] - 1;
					$validData['flowstate'] = 5;
				}

				$saveResult = $model->approval($validData);
				break;
			case 'complete':

				if ((int)$validData['flow'] == (int)$validData['max_flow']) {
					$validData['flow_id'] = (int)$validData['flow'];
					$validData['flowstate'] = 1;
					$validData['message'] = '';
				}

				$saveResult = $model->approval($validData);
				break;
			default:
				// 是否允许审核者编辑
				$comParams = JComponentHelper::getParams('com_sixeworkflow');
				$permit_edit_by_checker = $comParams->get('permit_edit_by_checker', 0);
				$isAuthor = !empty($user->id) && !empty($validData['created_by']) && $user->id == $validData['created_by'];
				$userflow = $model->getUserFlow($validData['catid']);
				$chkPermitSave = $recordId == 0 || (($permit_edit_by_checker || $isAuthor) && in_array($validData['flow'], $userflow));
				if (!$chkPermitSave) {
					// Redirect back to the edit screen.
					$this->setRedirect(
						\JRoute::_(
							'index.php?option=' . $this->option . '&view=' . $this->view_item
							. $this->getRedirectToItemAppend($recordId, $urlVar), false
						)
					);
					return false;
				}

				$saveResult = $model->save($validData);
				break;
		}


		// Attempt to save the data.
		if (!$saveResult) {
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Redirect back to the edit screen.
			$this->setError(\JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}

		$this->setMessage(\JText::_($this->text_prefix . '_SAVE_SUCCESS'));
		$messageid = $model->getState($this->context . '.id');
		switch ($task) {
			case 'submit':

				$this->setMessage(\JText::_($this->text_prefix . '_SUBMIT_SUCCESS'));
				$message = new stdclass;
				$message->content_id = $messageid;
				$message->flow = $validData['flow'];
				$message->type = 1;
				$message->from_user = $user->id;
				$message->message = $validData['message'];
				//$model->SendMessage($message);
				if ((int)$validData['flow'] == (int)$validData['max_flow']) {
					$this->setMessage(\JText::_($this->text_prefix . '_COMPLETE_SUCCESS'));
				}

				break;
			case 'reject':
				$this->setMessage(\JText::_($this->text_prefix . '_REJECT_SUCCESS'));
				$message = new stdclass;
				$message->content_id = $messageid;
				$message->flow = $validData['flow'];
				$message->type = 2;
				$message->from_user = $user->id;
				$message->message = $validData['message'];

				//$model->SendMessage($message);
				break;
			case 'complete' :
				$this->setMessage(\JText::_($this->text_prefix . '_COMPLETE_SUCCESS'));
				break;
			default:
				break;
		}


		// Save succeeded, so check-in the record.
		if ($checkin && $model->checkin($validData[$key]) === false) {
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Check-in failed, so go back to the record and display a notice.
			$this->setError(\JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				\JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}


		// Redirect the user and adjust session state based on the chosen task.
		switch ($task) {
			case 'apply':
				// Set the record data in the session.
				$recordId = $model->getState($this->context . '.id');
				$this->holdEditId($context, $recordId);
				$app->setUserState($context . '.data', null);
				$model->checkout($recordId);

				// Redirect back to the edit screen.
				$this->setRedirect(
					\JRoute::_(
						'index.php?option=' . $this->option . '&view=' . $this->view_item
						. $this->getRedirectToItemAppend($recordId, $urlVar), false
					)
				);
				break;

			case 'save2view':
				// Clear the record id and data from the session.
				$recordId = $model->getState($this->context . '.id');
				$this->releaseEditId($context, $recordId);
				$app->setUserState($context . '.data', null);
				$pview_link = JRoute::_(ContentHelperRoute::getArticleRoute($recordId, $validData['catid'], $validData['language']), false);
				$model->checkout($recordId);
				$this->setMessage(\JText::_($this->text_prefix . '_SAVE_SUCCESS') . '<script>jQuery(document).ready(function(){jQuery("#preview-button").click();});</script>');

				// Redirect back to the edit screen.
				// 

				$this->setRedirect(
					\JRoute::_(
						'index.php?option=' . $this->option . '&view=' . $this->view_item
						. $this->getRedirectToItemAppend($recordId, $urlVar), false
					)
				);

				break;

			default:
				// Clear the record id and data from the session.
				$this->releaseEditId($context, $recordId);
				$app->setUserState($context . '.data', null);

				$url = 'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend();

				// Check if there is a return value
				$return = $this->input->get('return', null, 'base64');

				if (!is_null($return) && \JUri::isInternal(base64_decode($return))) {
					$url = base64_decode($return);

				}


				// Redirect to the list screen.
				$this->setRedirect(\JRoute::_($url, false));
				break;
		}

		// Invoke the postSave method to allow for the child class to access the model.
		$this->postSaveHook($model, $validData);

		return true;
	}


	/**
	 * Method to reload a record.
	 *
	 * @param   string $key The name of the primary key of the URL variable.
	 * @param   string $urlVar The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return  void
	 *
	 * @since   3.8.0
	 */
	public function reload($key = null, $urlVar = 'a_id')
	{
		return parent::reload($key, $urlVar);
	}


	public function checked()
	{
		//JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$id = $app->input->getInt('a_id', 0);
		$model = $this->getModel();
		$data = $model->getFlowContent($id);
		$userflow = $model->getFlow($data->catid, $user->id);

		if (in_array($data->flow, $userflow)) {
			if ((int)$data->flow < (int)$data->max_flow) {
				$data->flow_id = (int)$data->flow;
				$data->id = $id;
				$data->flow = (int)$data->flow + 1;
				$data->flowstate = 4;
				$data->message = '';
				$message = '_SUBMIT_SUCCESS';
			} elseif ((int)$data->flow == (int)$data->max_flow) {
				$data->flow_id = (int)$data->flow;
				$data->flowstate = 1;
				$data->message = '';
				$message = '_COMPLETE_SUCCESS';
			}
			$data = (array)$data;
			$return = $model->save($data);
		} else {
			$message = '_SUBMIT_ERROR';
			$messagetype = 'error';
		}

		$this->setMessage(\JText::_($this->text_prefix . $message), $messagetype);

		$this->setRedirect(JRoute::_($this->getReturnPage(), false));
		//$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId,'a_id'), false));

	}


	public function checkin()
	{
		// Check for request forgeries.
		//$this->checkToken();

		$id = $this->input->get('a_id');

		$model = $this->getModel();
		$return = $model->checkin($id);

		if ($return === false) {
			// Checkin failed.
			$message = \JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError());
			$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list, false), $message, 'error');

			return false;
		} else {
			// Checkin succeeded.
			$this->setRedirect(JRoute::_($this->getReturnPage(), false));

			return true;
		}
	}
}
