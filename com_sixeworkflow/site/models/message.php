<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Rainbowpay model.
 *
 * @since  1.6
 */
class SixeWorkFlowModelMessage extends JModelItem
{
    public $_item;
   
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return void
	 *
	 * @since    1.6
	 *
	 */
	protected function populateState()
	{
		$app=JFactory::getApplication();
		$id = $app->input->get('id');
		$this->setState('message.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();


		$this->setState('params', $params);
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer $id The id of the object to get.
	 *
	 * @return  mixed    Object on success, false on failure.
     *
     * @throws Exception
	 */
	public function getItem($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('message.id');
			}
			if($id > 0)
			{
				$db = $this->getDbo();
				$query = $db->getQuery(true)
				->select('a.*,u.name AS from_name,c.title,c.catid,c.language')
				->from('#__workflow_messages AS a')
				->join('LEFT','#__users AS u ON u.id=a.from_user')
				->join('LEFT','#__content AS c ON c.id=a.content_id')
				->where('a.id='.$id);
				$db->setQuery($query);
				
				$data = $db->loadObject();
				
				if (empty($data))
				{
					return JError::raiseError(404, JText::_('COM_CONTENT_ERROR_ARTICLE_NOT_FOUND'));
				}
				else
				{

					$this->_item=$data;
					if($this->_item->is_read==0)
					{
						$table=$this->getTable();
						$table->load($data->id);
						$table->is_read=1;
						$table->store();
					}
				}
			}
			
		}

		return $this->_item;
	}



public function getTable($type = 'Message', $prefix = 'SixeWorkFlowTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_sixeworkflow/tables');

		return JTable::getInstance($type, $prefix, $config);
	}



	

}
