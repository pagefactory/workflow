<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */



defined('JPATH_PLATFORM') or die;
use Joomla\Utilities\ArrayHelper;


//JFormHelper::loadFieldClass('list');

/**
 * Field to load a dropdown list of available user groups
 *
 * @since  3.2
 */
class JFormFieldFlowState extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   3.2
	 */
	protected $type = 'FlowState';

	

	/**
	 * Method to attach a JForm object to the field.
	 *
	 * @param   \SimpleXMLElement  $element  The SimpleXMLElement object representing the `<field>` tag for the form field object.
	 * @param   mixed              $value    The form field value to validate.
	 * @param   string             $group    The field name group control value. This acts as an array container for the field.
	 *                                       For example if the field has name="foo" and the group value is set to "bar" then the
	 *                                       full field name would end up being "bar[foo]".
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.7.0
	 */
	public function setup(SimpleXMLElement $element, $value, $group = null)
	{
	
		return parent::setup($element, $value, $group);
	}

	/**
	 * Method to get the options to populate list
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   3.2
	 */
	protected function getOptions()
	{
		$options = array();
		$app=JFactory::getApplication();
		$user=JFactory::getUser();
		$id=$app->input->get('a_id',0);
		if($app->input->get('option','') =='com_sixeworkflow' && $app->input->get('view','') == 'articleform' && $id > 0 )
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('DISTINCT a.flow_id,a.title,c.flow,c.start_flow,c.state,c.created_by,wc.workflow_id')
				  ->from('#__workflow_users AS a')
				  ->join('LEFT','#__workflow_categories AS wc ON wc.workflow_id=a.workflow_id')
				  ->join('LEFT','#__workflow_contents AS c ON c.catid=wc.category_id')
				  ->where('c.content_id='.(int)$id);
				  $query->order('a.flow_id ASC');
				  $db->setQuery($query);

				try
				{
					$options = $db->loadObjectList();
				}
				catch (RuntimeException $e)
				{
					JError::raiseWarning(500, $e->getMessage());
				}

		}

		return $options;
	}

	protected function getInput()
	{
		$options = (array) $this->getOptions();
		
		$start=$options[0]->start_flow;
		$html=array();
		$html[]='<input type="hidden" name="'.$this->name.'" id="'.$this->id.'" value="'.$this->value.'" />';
		$html[]='<ul class="workflows-list flowstate">';
		foreach($options as $key=>$option)
		{
			$li_class='';
			$icon_class='icon-clock';
			if($option->flow_id < $start)
			{
				$li_class.=' stop';
				$icon_class='icon-stop-circle';
			}
			else
			{
				//$icon_class='icon-ok';
				if($option->flow_id == $start)
				{
					$li_class.=' start';
				}
				if($option->flow_id < $option->flow)
				{
					$li_class.=' ok';
					$icon_class='icon-checkmark';
				}
				if($option->flow_id == $option->flow)
				{
					$li_class.=' active';
					if($option->state==3)
					{
						$li_class.=' edit';
						$icon_class='icon-pencil';
					}
					elseif($option->state==4)
					{
						$li_class.=' submit';
						$icon_class='icon-share';
					}
					elseif($option->state==5)
					{
						$li_class.=' reject';
						$icon_class='icon-reply';
					}
					elseif($option->state==1)
					{
						$li_class.=' ok';
					$icon_class='icon-checkmark';
					}
				}


			}
			

			$html[]='<li class="'.$li_class.'">';

			$html[]='<div class="state-item">';
			$html[]='<i class="state-icon '.$icon_class.'" ></i>';
			$html[]='<strong class="state-text">'.$option->title.'</strong>';
			$html[]='</div>';
			 if($key+1 < count($options))
			{
			$html[]='<div class="center"><i class="arrow icon-arrow-down-4"></i></div>';
			}						
			$html[]='</li>';
		}
		$html[]='</ul>';
		return implode($html);
	}



}
