<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Rainbowpay records.
 *
 * @since  1.6
 */
class SixeWorkFlowModelMessages extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'is_read', 'a.is_read',
				'type', 'a.type',
				'created','a.created',
				'title', 'username',

			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		

		// List state information.
		parent::populateState($ordering, $direction);
		$app = Factory::getApplication();

		$params = $app->getParams();
		$menuParams = new Registry;

		if ($menu = $app->getMenu()->getActive())
		{
			$menuParams->loadString($menu->params);
		}

		$mergedParams = clone $menuParams;
		$mergedParams->merge($params);

		$this->setState('params', $mergedParams);



  	$orderCol = $app->input->get('filter_order', 'a.id');

		if (!in_array($orderCol, $this->filter_fields))
		{
			$orderCol = 'a.id';
		}

		$this->setState('list.ordering', $orderCol);

		$listOrder = $app->input->get('filter_order_Dir', 'ASC');

		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', '')))
		{
			$listOrder = 'ASC';
		}

		$this->setState('list.direction', $listOrder);


       // $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
       // $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', $params->get('display_num'), 'int');
		$type = $app->input->get('filter_type', 0, 'int');
		$this->setState('filter.type', $type);
		$start = $app->input->get('limitstart', 0, 'int');
		$limit = $app->input->get('limit', $params->get('display_num'), 'int');
        if ($limit == 0)
        {
            $limit = $app->get('list_limit', 0);
        }
        //$app->setUserState('com_rms.filter.search',$search);

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		$user=JFactory::getUser();
		// Select the required fields from the table.
		$query
			->select('DISTINCT a.*,u.name AS username,c.title');

		$query->from('`#__workflow_messages` AS a')
			  ->join('LEFT','#__users AS u ON u.id=a.from_user')
			  ->join('LEFT','#__content AS c ON c.id=a.content_id')
			  ->where('a.to_user='.$user->id);

		$type=$this->getState('filter.type',0);
		if($type > 0)
		{
			$query->where('a.type='.$type);
		}
		$query->order($this->getState('list.ordering', 'a.id') . ' ' . $this->getState('list.direction', 'DESC'));
		return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		
		$items = parent::getItems();
		$params=$this->getState('params');
		if(!empty($items))
		{
			
		}

		return $items;
	}


	public function getTypes()
	{
		$user=JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
		->select('DISTINCT a.type,COUNT(*) AS count')
		->from('#__workflow_messages AS a')
		->group('a.type')
		->order('a.type ASC')
		->where('a.to_user='.$user->id);
		//->where('a.is_read=0');
		$db->setQuery($query);
		return $db->loadObjectList();
		
	}


	

}
