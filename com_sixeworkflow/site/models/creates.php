<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper;

jimport('joomla.application.component.modellist');
JLoader::register('SixeWorkFlowModelArticles', JPATH_SITE . '/components/com_sixeworkflow/models/articles.php');
/**
 * Methods supporting a list of Rainbowpay records.
 *
 * @since  1.6
 */
class SixeWorkFlowModelCreates extends SixeWorkFlowModelArticles
{
	

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		

		// List state information.
		parent::populateState($ordering, $direction);
		
     


       
       $this->setState('filter.state',-1);

       
	}

	protected function getListQuery()
	{
		// Create a new query object.
		$user=JFactory::getUser();
		$groups=implode(',',$user->groups);
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		/*
 		$subquery='(SELECT COUNT(*) FROM #__workflow_users WHERE workflow_id=wc.workflow_id AND flow_id>c.start_flow AND 
 		((type=1 AND user_id IN('.$groups.')) OR (type=2 AND user_id='.$user->id.'))) > 0 '; 
 		*/
		$query
			->select('a.id,a.title,a.alias,a.catid,c.state,a.created,a.attribs,a.language,a.created_by_alias,a.created_by,a.access,a.publish_up,a.publish_down,a.hits,a.checked_out,a.checked_out_time,c.checked AS checked,c.rejected AS rejected,c.flow,c.start_flow');

		$query->from('#__workflow_contents AS c')
			->join('INNER','#__workflow_categories AS wc ON wc.category_id=c.catid')
			->where('c.created_by='.$user->id)
			//->where('((c.created_by='.$user->id.' ) OR ('.$subquery.' ))')
		->join('INNER','#__content AS a ON a.id=c.content_id')
		->select("CASE WHEN a.created_by_alias > ' ' THEN a.created_by_alias ELSE u.name END AS author")
		->join('LEFT', '#__users AS u ON u.id = a.created_by')
		->select('ucheck.name AS checked_name,ureject.name AS rejected_name')
		->join('LEFT','#__users AS ucheck ON ucheck.id=c.checked_by')
		->join('LEFT','#__users AS ureject ON ureject.id=c.rejected_by');
		$state=$this->getState('filter.state',-1);
		
		if(is_numeric($state) && $state > -1)
		{
			$query->where('c.state='.$state);
		}
		if(is_array($state) && count($state) && !in_array(-1,$state))
		{
			$query->where('c.state IN ('.implode(',',$state).')');
		}
		
		
		$query->order($this->getState('list.ordering', 'a.id') . ' ' . $this->getState('list.direction', 'DESC'));
		
		return $query;
	}


	public function getItems()
	{
		$user=JFactory::getUser();
		$items=parent::getItems();
		foreach($items as &$item)
		{
			$item->flows=$this->getItemFlows($item->catid,$item->id);
			$item->userflow=$this->getFlow($item->catid,$user->id);
		}
		return $items;
	}



}
