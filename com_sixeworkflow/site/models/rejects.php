<?php
/**
 * @author       Sixe Team
 * @email        info@eee-eee.com
 * @url          http://www.eee-eee.com
 * @copyright    Copyright (C) 2010 - 2019 Sixe Information Technology Limited. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 * @date         2019/10/01 10:00
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper;

jimport('joomla.application.component.modellist');
JLoader::register('SixeWorkFlowModelArticles', JPATH_SITE . '/components/com_sixeworkflow/models/articles.php');
/**
 * Methods supporting a list of Rainbowpay records.
 *
 * @since  1.6
 */
class SixeWorkFlowModelRejects extends SixeWorkFlowModelArticles
{
	

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		

		// List state information.
		parent::populateState($ordering, $direction);
	       
       $this->setState('filter.state',5);

        
	}


}
